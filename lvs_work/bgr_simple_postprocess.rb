def lvs_postprocess ml2
  puts netlist
  top_circuit = l2n_data.netlist.circuit_by_name(source.layout.top_cell.name)
  net = top_circuit.net_by_name("VOUT")

  probe_point = RBA::Point::new(-2946000,-2840000)
  net = l2n_data.probe_net(ml2.data, probe_point)
  net && puts("Net at #{probe_point}: #{net.name}")
  
  puts $shapes = l2n_data.shapes_of_net(net, ml2.data, true)
end  
